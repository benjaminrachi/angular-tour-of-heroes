import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Hero } from '../../models/hero';
import {HEROES} from '../../mock-heroes';
import {MessageService} from '../message/message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private messageService : MessageService) { }

  getHeroes() : Observable<Hero[]>{
    this.messageService.add("HeroService: heroes fetched and will be delivered");
    return of (HEROES);
  }
}
