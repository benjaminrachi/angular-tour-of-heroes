import { Component, OnInit } from '@angular/core';
import { Hero } from '../../models/hero';
import { HeroService } from '../../services/hero/hero.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: Hero[]; // not observable
  selectedHero: Hero;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  getHeroes(): void { // heroes is not an observable
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  //heroes$: Observable<Hero[]>;
  // getHeroes() : void{ // heroes$ is an observable
  //   this.heroes$ =this.heroService.getHeroes();
  // }
}
